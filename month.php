<?php

require 'db.php';

$month = date('m');
$year = date('Y');

$query = "SELECT DATE_FORMAT(cost_date,'%D %b') as cost_date,SUM(cost_amount) as total_amount FROM tbl_cost WHERE month(cost_date)=:month && year(cost_date)=:year GROUP BY cost_date";
$stmt = $connection->prepare($query);
$stmt->execute([':month' => $month, ':year' => $year]);
$data = $stmt->fetchAll(PDO::FETCH_OBJ);

if (isset($_POST['submit'])) {
    $month = $_POST['month'];
    $year = $_POST['year'];

    $query = "SELECT DATE_FORMAT(cost_date,'%D %b') as cost_date, SUM(cost_amount) as total_amount FROM tbl_cost WHERE month(cost_date)=:month && year(cost_date)=:year GROUP BY cost_date";
    $stmt = $connection->prepare($query);
    $stmt->execute([':month' => $month, ':year' => $year]);
    $data = $stmt->fetchAll(PDO::FETCH_OBJ);
}

$cost_date = array();
$total_amount = array();

foreach ($data as $value) {
    $cost_date[] = $value->cost_date;
    $total_amount[] = $value->total_amount;
}

?>

<?php include 'header.php';?>


<div>
<h1>Search for any month expences date wise</h1>
  <form action="" method="post">
        <div>
            <label for="">Month:</label>
            <input value="<?=$month;?>" type="text" name="month">
        </div>
        <div>
            <label for="">Year:</label>
            <input value="<?=$year;?>" type="text" name="year">
        </div>
        <div>
            <input type="submit" name="submit" value="Search">
        </div>
  </form>
</div>
<div>
<h1>Expences of the month <?php echo $month . ', ' . $year; ?></h1>
    <table>
        <thead>
            <tr>
                <th>Cost Date</th>
                <th>Cost Amount</th>
            </tr>
        </thead>
        <?php
foreach ($data as $value) {
    ?>
        <tr>
            <td><?php echo $value->cost_date; ?></td>
            <td><?php echo $value->total_amount; ?></td>
        </tr>
        <?php }?>
    </table>
</div>


<!-- code for charts -->


<div class="container">
    <canvas id="myChart"></canvas>
</div>

<script>
var ctx = document.getElementById('myChart').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'bar',

    // The data for our dataset
    data: {
        labels: <?php echo json_encode($cost_date); ?>,
        datasets: [{
            label: 'Daily Cost of a month',
            data: <?php echo json_encode($total_amount); ?>,
            backgroundColor:[
            'rgba(255, 99, 132, 0.6)',
            'rgba(54, 162, 235, 0.6)',
            'rgba(255, 206, 86, 0.6)',
            'rgba(75, 192, 192, 0.6)',
            'rgba(153, 102, 255, 0.6)',
            'rgba(255, 159, 64, 0.6)',
            'rgba(255, 99, 132, 0.6)'
          ],
          borderWidth:1,
          borderColor:'#777',
          hoverBorderWidth:3,
          hoverBorderColor:'#000'
        }]
    },

    // Configuration options go here
    options: {}
});

</script>

<?php include 'footer.php';?>