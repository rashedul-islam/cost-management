<?php

require 'db.php';
$year = date('Y');

$query = "SELECT DATE_FORMAT(cost_date,'%M') as month_name,
                 SUM(cost_amount) as total_amount
         FROM tbl_cost WHERE year(cost_date)=:year GROUP BY month(cost_date)";
$stmt = $connection->prepare($query);
$stmt->execute([':year' => $year]);
$data = $stmt->fetchAll(PDO::FETCH_OBJ);

if (isset($_POST['submit'])) {
    $year = $_POST['year'];

    $query = "SELECT DATE_FORMAT(cost_date,'%M') as month_name,SUM(cost_amount) as total_amount FROM tbl_cost WHERE year(cost_date)=:year GROUP BY month(cost_date)";
    $stmt = $connection->prepare($query);
    $stmt->execute([':year' => $year]);
    $data = $stmt->fetchAll(PDO::FETCH_OBJ);

}

$month_name = array();
$amount = array();

foreach ($data as $value) {
    $month_name[] = $value->month_name;
    $amount[] = $value->total_amount;
}
// print_r($month_name);
// print_r($amount);
// exit();

?>

<?php include 'header.php';?>

<div>
<h1>Search for expences of a Year</h1>
<form action="" method="post">
        <div>
            <label for="">Year:</label>
            <input value="<?=$year;?>" type="text" name="year">
        </div>
        <div>
            <input type="submit" name="submit" value="Search">
        </div>
  </form>
</div>
<div>
<h1>Expences of year <?php echo $year; ?></h1>
    <table>
        <thead>
            <tr>
                <th>Month</th>
                <th>Cost Amount</th>
            </tr>
        </thead>
        <?php
foreach ($data as $value) {
    ?>
            <tr>
                <td><?php echo $value->month_name; ?></td>
                <td><?php echo $value->total_amount; ?></td>
            </tr>

        <?php }?>
    </table>
</div>


<!-- code for charts -->


<div class="container">
    <canvas id="myChart"></canvas>
</div>

<script>
var ctx = document.getElementById('myChart').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'bar',

    // The data for our dataset
    data: {
        labels: <?php echo json_encode($month_name); ?>,
        datasets: [{
            label: 'Monthly Cost of a year',
            data: <?php echo json_encode($amount); ?>,
            backgroundColor:[
            'rgba(255, 99, 132, 0.6)',
            'rgba(54, 162, 235, 0.6)',
            'rgba(255, 206, 86, 0.6)',
            'rgba(75, 192, 192, 0.6)',
            'rgba(153, 102, 255, 0.6)',
            'rgba(255, 159, 64, 0.6)',
            'rgba(255, 99, 132, 0.6)'
          ],
          borderWidth:1,
          borderColor:'#777',
          hoverBorderWidth:3,
          hoverBorderColor:'#000'
        }]
    },

    // Configuration options go here
    options: {}
});

</script>

<?php include 'footer.php';?>